﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {

    public BeeMove beePrefab;
    public PlayerMove player;
    public PlayerMove player2;
    public int nBees = 50;
    public Rect spawnRect;    public float minBeePeriod;
    public float maxBeePeriod;
    private float beePeriod;
    private float i = 0.0f;

    // Use this for initialization
    void Start () {
        beePeriod = Random.Range(minBeePeriod, maxBeePeriod);
    }

    // Update is called once per frame
    void Update () {

        i += Time.deltaTime;

        if (i > beePeriod && nBees > 0) //to ensure that no more than 50 bees spawn
        {
            spawnBee();
            i = 0;
            nBees--;
            beePeriod = Random.Range(minBeePeriod, maxBeePeriod);
        }
        
    }


    public void spawnBee()
    {
        BeeMove bee = Instantiate(beePrefab);
        bee.transform.parent = transform;
        bee.gameObject.name = "Bee" + (50 - nBees); //to name bees appropriately

        // move the bee to a random position within
        // the spawn rectangle
        float x = spawnRect.xMin +
        Random.value * spawnRect.width;
        float y = spawnRect.yMin +
        Random.value * spawnRect.height;

        bee.transform.position = new Vector2(x, y);
    }
    public void DestroyBees(Vector2 centre, float radius)
    {
        // destroy all bees within ‘radius’ of ‘centre’
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            // BUG! the line below doesn’t work
            Vector2 v = (Vector2)child.position - centre;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
            }
        }
    }

    void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMin));
    }

}
