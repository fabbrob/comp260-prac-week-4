﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

    // public parameters with default values
    public float minSpeed, maxSpeed;
    public float minTurnSpeed, maxTurnSpeed;
    // private state
    private float speed;
    private float turnSpeed;
    public GameObject player;
    public GameObject player2;
    public Transform target1;
    public Transform target2;
    private Vector2 heading = Vector2.right;

    public ParticleSystem explosionPrefab;

    // Use this for initialization
    void Start () {

        player = GameObject.FindWithTag("Player");      //get help to see if you can code this better
        player2 = GameObject.FindWithTag("Player2");

        target1 = player.transform;
        target2 = player2.transform;

        // bee initially moves in random direction
        heading = Vector2.right;
        float angle = Random.value * 360;
        heading = heading.Rotate(angle);
        // set speed and turnSpeed randomly
        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed,
        Random.value);

    }

    // Update is called once per frame
    void Update () {
        // get the vector from the bee to the two targets
        Vector2 direction1 = target1.position - transform.position;
        Vector2 direction2 = target2.position - transform.position;
        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;

        if (direction1.magnitude > direction2.magnitude) // if target2 is closer than target1
        {
            // turn left or right
            if (direction2.IsOnLeft(heading))
            {
                // target on left, rotate anticlockwise
                heading = heading.Rotate(angle);
            }
            else
            {
                // target on right, rotate clockwise
                heading = heading.Rotate(-angle);
            }
        }
        else // if target1 is closer than target2
        {
            // turn left or right
            if (direction1.IsOnLeft(heading))
            {
                // target on left, rotate anticlockwise
                heading = heading.Rotate(angle);
            }
            else
            {
                // target on right, rotate clockwise
                heading = heading.Rotate(-angle);
            }
        }
        
        transform.Translate(heading * speed * Time.deltaTime);
    }

    void OnDestroy()
    {
        // create an explosion at the bee's current position
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;
        Destroy(explosion.gameObject, explosion.duration);
    }
    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);
        // draw target vector in yellow
        Gizmos.color = Color.yellow;
        Vector2 direction1 = target1.position - transform.position;
        Gizmos.DrawRay(transform.position, direction1);
        Vector2 direction2 = target2.position - transform.position;
        Gizmos.DrawRay(transform.position, direction2);
    }
}
